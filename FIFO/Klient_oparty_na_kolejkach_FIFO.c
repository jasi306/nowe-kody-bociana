/*
Klient oparty na kolejkach FIFO
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>

char katalog1[100];
char katalog2[100];
int in, out;


void ala(int i) {
    close(in);
    close(out);
    unlink(katalog1);
    unlink(katalog2);
    exit(0);
}

void create_fifo(char* katalog){
    if (mknod(katalog, S_IFIFO | 0600, 0) == -1) {
        perror("Serwer: Blad tworzenia kolejki FIFO.\n");
        exit(-1);
    }
}

void open_fifo(char* katalog, int* file, mode_t mode){
    if ((*file = open(katalog, mode)) == -1 ) {
        perror("Serwer: Blad otwarcia kolejki FIFO.\n");
        close(in);
        close(out);
        exit(-1);
    }
}

int main() {
    pid_t my_pid = getpid();
    char server[100];
    double a, b;
    // ustawianie funkcji do czyszczenia w razie przedwczesnego zgonu
    signal(SIGTERM, ala);
    signal(SIGINT, ala);
    // tworzenie polaczenia do głównego wątku serwera
    sprintf(server, "/tmp/FIFO-TMP-SERWER");
    open_fifo(server, &out, O_WRONLY);
    // tworzenie kolejek FIFO do komunikacji z dzieckiem servera na podstawie pid
    sprintf(katalog1, "/tmp/FIFO-TMP-%d-%d", 1, my_pid);;
    create_fifo(katalog1);
    sprintf(katalog2, "/tmp/FIFO-TMP-%d-%d", 2, my_pid);;
    create_fifo(katalog2);
    // wysyłanie swojego pid do servera, bo na jego podstawie utworzona jest nazwa kolejek FIFO
    write(out, &my_pid, sizeof(pid_t));
    close(out);
    // otwieranie kolejek do komunikacji z dzieckiem servera
    open_fifo(katalog1, &out, O_WRONLY);
    open_fifo(katalog2, &in , O_RDONLY);
    // rozmowa z dzieckiem servera
    printf("Podaj liczbe: ");
    scanf("%lf", &a);
    write(out, &a, sizeof(double));
    read(in, &b, sizeof(double));
    printf("Wynik obliczen %lf^2 = %lf.\n", a, b);
    // zamykanie
    close(in);
    close(out);
    unlink(katalog1);
    unlink(katalog2);
    return 0;
}
