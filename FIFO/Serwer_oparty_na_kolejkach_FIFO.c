/*
Serwer oparty na kolejkach FIFO
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <signal.h>
#include <string.h>

char katalog[100];
int in, out;

void ala(int i) {
    close(in);
    close(out);
    unlink(katalog);
    exit(0);
}

void create_fifo(char* katalog){
    if (mknod(katalog, S_IFIFO | 0600, 0) == -1) {
        perror("Serwer: Blad tworzenia kolejki FIFO.\n");
        exit(-1);
    }
}

void open_fifo(char* katalog, int* file, mode_t mode){
    if ((*file = open(katalog, mode)) == -1 ) {
        perror("Serwer: Blad otwarcia kolejki FIFO.\n");
        close(in);
        close(out);
        exit(-1);
    }
}

void child_proces(int client_pid){
    double a, b;
    // otwórz kolejki do klienta na podstawie jego pid
    sprintf(katalog, "/tmp/FIFO-TMP-%d-%d", 1, client_pid);;
    open_fifo(katalog, &in , O_RDONLY);
    sprintf(katalog, "/tmp/FIFO-TMP-%d-%d", 2, client_pid);
    open_fifo(katalog, &out, O_WRONLY);
    // zrób matematykę i odeślij wynik
    read(in, &a, sizeof(double));
    b = a * a;
    write(out, &b, sizeof(double));
    // zamknij połączenie
    close(in);
    close(out);
}

int main() {
    pid_t client_pid, pid;
    // ustawianie funkcji do czyszczenia w razie przedwczesnego zgonu
    signal(SIGTERM, ala);
    signal(SIGINT, ala);
    // tworzenie głownej kolejki
    sprintf(katalog, "/tmp/FIFO-TMP-SERWER");
    create_fifo(katalog);
    // otwieranie połączenia do kolejki
    open_fifo(katalog, &in, O_RDONLY);
    open_fifo(katalog, &out, O_WRONLY); // bez tego nie działa? czemu?
    // głowna pętla servera
    while (1) {
        //wczytaj pid klienta, bo na jego podstawie utworzył kolejki do komunikacji
        read(in, &client_pid, sizeof(pid_t));
        // utwórz proces potomny
        if ((pid = fork()) == -1) {
            perror("Wywolanie funkcji fork nie powiodlo sie.\n");
            return -1;
        } 
        if (pid == 0) {
            // obsłóż klienta w procesie potomnym
            child_proces(client_pid);
            return 0;
        }
    }
}
