/*
Serwer sekwencyjny oparty na semaforach i pamieci wspoldzielonej (wersja standardowa). Do synchronizacji dostepu do pamieci uzywane sa dwa semafory: 0 (wartosc 0, gdy klient nie ma dostepu do wspolnej pamieci i warotsc 1, gdy klient ma dostep do wspolnej pamieci), 1 (wartosc 0, gdy serwer nie ma dostepu do wspolnej pamieci i wartosc 1, gdy serwer ma dostep do wspolnej pamieci).
*/

#include<stdio.h>
#include<string.h>
#include<sys/sem.h>
#include<sys/shm.h>
#include<stdlib.h>
#include<signal.h>
#include<unistd.h>
#include<fcntl.h>
#include<errno.h>
#include<sys/types.h>

typedef struct sembuf bufor_semaforow;
bufor_semaforow ustawienia_semafora;
int id_semafor;
int id_pamieci;
char *pamiec_wspolna;
int czy_skasowac = 1;
char sciezka[] = "/tmp/roboczy.sem_serwer";
key_t klucz_semafory, klucz_pamiec_wspolna;


void close_all(){
    shmdt(pamiec_wspolna);
    shmctl(id_pamieci, IPC_RMID, NULL);
    semctl(id_semafor, 0, IPC_RMID, NULL);
    if(czy_skasowac)
        unlink(sciezka);
}

void al(int i) {
    if (i == SIGTERM || i == SIGINT) {
        printf("Serwer: Koniec pracy serwera.\n");
        close_all();
        exit(0);
    }
}

void touch(){
    int plik = 0;
    if (access(sciezka, F_OK) == 0) {
        czy_skasowac = 0;
    }
    if (czy_skasowac && ((plik = open(sciezka, O_CREAT | O_EXCL, 0600)) == -1)) { //nie mam acc i nie udało się utworzyć
        fprintf(stderr, "Serwer: Blad utworzenia pliku %s: %s.\n", sciezka, strerror(errno));
        exit(-1);
    }
    close(plik);
}

void post(int id_semafora, int id){
    ustawienia_semafora.sem_num = id;
    ustawienia_semafora.sem_op = 1;
    ustawienia_semafora.sem_flg = 0;
    if (semop(id_semafora, &ustawienia_semafora, 1) == -1) {
        perror("Klient: Blad oczekiwania na otwarcie semafora do odczytu dla serwera (zwolnienie dla serwera).\n");
        close_all();
        exit(-1);
    }       
}

void wait(int id_semafora, int id){
    ustawienia_semafora.sem_num = id; 
    ustawienia_semafora.sem_op = -1;
    ustawienia_semafora.sem_flg = 0;
    if (semop(id_semafora, &ustawienia_semafora, 1) == -1) {
        perror("Klient: Blad oczekiwania na otwarcie semafora do zapisu dla klienta (oczekiwanie klienta), funkcja semop.\n");
        close_all();
        exit(-1);
    }
}


int main(int argc, char *argv[]) {
    
    bufor_semaforow ustaw_semafory;
    signal(SIGTERM, al);
    signal(SIGINT, al);
    printf("t1\n");
    touch();
    printf("t\n");
    
    if ((klucz_semafory = ftok(sciezka, 0)) == -1 || (klucz_pamiec_wspolna = ftok(sciezka, 1)) == -1) {
        perror("Serwer: Blad funkcji ftok.\n");
        close_all();
        return -1;
    }
    if ((id_pamieci = shmget(klucz_pamiec_wspolna, 50 * sizeof(char), 0600 | IPC_CREAT | IPC_EXCL)) == -1) {
        perror("Serwer: Blad funkcji shmget.\n");
        close_all();
        return -1;
    } 
    if ((id_semafor = semget(klucz_semafory, 2, 0600 | IPC_CREAT | IPC_EXCL)) == -1) {
        perror("Serwer: Blad funkcji semget.\n");
        close_all();
        return -1;
    }

    pamiec_wspolna = shmat(id_pamieci, (char *) 0, 0);
    u_short wartosci_poczatkowe_semaforow[] = {1, 0};
    
    if ((semctl(id_semafor, 0, SETALL, wartosci_poczatkowe_semaforow)) == -1) {
        perror("Serwer: Blad funkcji semctl.\n");
        close_all();
        return -1;
    }
    while (1) {
        post(id_semafor, 0);
        char *wiadomosc = strdup(pamiec_wspolna);
        printf("Serwer: Wiadomosc od klienta: %s.\n", wiadomosc);
        free(wiadomosc);
        wait(id_semafor, 1);
    }
}