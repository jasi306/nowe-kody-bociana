/*
Klient oparty na semaforach i pamieci wspoldzielonej dla serwera sekwencyjnego (wersja standardowa). Do synchronizacji dostepu do pamieci uzywane sa dwa semafory: 0 (wartosc 0, gdy klient nie ma dostepu do wspolnej pamieci i warotsc 1, gdy klient ma dostep do wspolnej pamieci), 1 (wartosc 0, gdy serwer nie ma dostepu do wspolnej pamieci i wartosc 1, gdy serwer ma dostep do wspolnej pamieci).
*/

#include<sys/sem.h>
#include<sys/shm.h>
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<unistd.h>

typedef struct sembuf bufor_semaforow;
bufor_semaforow ustawienia_semafora;
key_t klucz_semafora, klucz_pamiec_wspolna;
int id_semafora;
int id_pamieci;
char *pamiec_wspolna;

void close_all(){
    shmdt(pamiec_wspolna);
}

void al(int i) {
    if (i == SIGTERM || i == SIGINT) {
        printf("Serwer: Koniec pracy serwera.\n");
        close_all();
        exit(0);
    }
}

void post(int id_semafora, int id){
    int rc;
    ustawienia_semafora.sem_num = id;
    ustawienia_semafora.sem_op = 1;
    ustawienia_semafora.sem_flg = 0;
    if ((rc=semop(id_semafora, &ustawienia_semafora, 1)) == -1) {
        
        perror("Klient: Blad oczekiwania na otwarcie semafora do odczytu dla serwera (zwolnienie dla serwera).\n");
        close_all();
        exit(-1);
    }   
    printf("%d\n",rc);    
}

void wait(int id_semafora, int id){
    int rc;
    ustawienia_semafora.sem_num = id; 
    ustawienia_semafora.sem_op = -1;
    ustawienia_semafora.sem_flg = 0;
    if ((rc = semop(id_semafora, &ustawienia_semafora, 1)) == -1) {
        perror("Klient: Blad oczekiwania na otwarcie semafora do zapisu dla klienta (oczekiwanie klienta), funkcja semop.\n");
        close_all();
        exit(-1);
    }
    printf("%d\n",rc);
}

int main(int argc, char *argv[]) {
    char sciezka[] = "/tmp/roboczy.sem_serwer";
    int liczba_sekund;
    int czy_czekac = 0;
    pid_t pid = getpid();

    if (argc > 1) {
        czy_czekac = 1;
        liczba_sekund = strtol(argv[1], NULL, 10);
    }

    if ((klucz_semafora = ftok(sciezka, 0)) == -1 || (klucz_pamiec_wspolna = ftok(sciezka, 1)) == -1) {
        perror("Klient: Blad funkcji ftok.\n");
        close_all();
        return -1;
    } 
    if ((id_semafora = semget(klucz_semafora, 2, 0600)) == -1) {
        perror("Klient: Blad funkcji semget.\n");
        close_all();
        return -1;
    } 
    if ((id_pamieci = shmget(klucz_pamiec_wspolna, 50 * sizeof(char), 0600)) == -1) {
        perror("Klient: Blad funkcji shmget.\n");
        close_all();
        return -1;
    }
    printf("przedW");
    wait(id_semafora, 0);
    printf("poW");
    pamiec_wspolna = shmat(id_pamieci, (char *) 0, 0);

    printf("Klient: Moj numer procesu to %d. Oczekuje na mozliwosc zapisu.\n", pid);
    char *wiadomosc = (char *) malloc(100 * sizeof(char));
    sprintf(wiadomosc, "Moj numer procesu to %d", pid);
    strcpy(pamiec_wspolna, wiadomosc);
    printf("Klient: Wiadomosc zapisana.\n");
    free(wiadomosc);

    shmdt(pamiec_wspolna);
    if (czy_czekac) {
        printf("Klient: Oczekuje %d sekund na zwolnienie semafora do czytania dla serwera.\n", liczba_sekund);
        sleep(liczba_sekund);
    }
    post(id_semafora, 1);
    close_all();
    return 0;        
}